namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Admin")]
    public partial class Admin
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string username { get; set; }

        [Required]
        [StringLength(50)]
        public string email { get; set; }

        [Required]
        [StringLength(50)]
        public string password { get; set; }

        [Required]
        [StringLength(50)]
        public string salt { get; set; }

        [Required]
        [StringLength(75)]
        public string fullname { get; set; }

        [Required]
        [StringLength(250)]
        public string address { get; set; }

        public int phone { get; set; }

        public int gender { get; set; }

        public DateTime dob { get; set; }

        public int role { get; set; }

        public int status { get; set; }

        public DateTime createtAt { get; set; }

        public DateTime updateAt { get; set; }
    }
}
