namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Category")]
    public partial class Category
    {
        public int ID { get; set; }

        [Required]
        [StringLength(100)]
        public string title { get; set; }

        [Required]
        [StringLength(100)]
        public string name { get; set; }

        [Required]
        [StringLength(250)]
        public string avatar { get; set; }

        public int status { get; set; }

        public int updateBy { get; set; }

        public DateTime createAt { get; set; }

        public DateTime updateAt { get; set; }

        public int createBy { get; set; }

        public int role { get; set; }
    }
}
