﻿namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Post")]
    public partial class Post
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Tiêu đề không được bỏ trống")]
        [StringLength(50)]
        public string title { get; set; }
        [Required(ErrorMessage = "Id danh mục không được bỏ trống")]
        public int categoryID { get; set; }
        [Required(ErrorMessage = "Nội dung không được bỏ trống")]
        public string content { get; set; }
        [Required(ErrorMessage = "Ảnh không được bỏ trống")]
        [StringLength(250)]
        public string avatar { get; set; }
        [Required(ErrorMessage = "Trạng thái không được bỏ trống")]
        public int status { get; set; }

        [Required]
        public DateTime createAt { get; set; }

        [Required]
        public DateTime updateAt { get; set; }

        [Required]
        public int createBy { get; set; }

        [Required]
        public int updateBy { get; set; }
    }
}
