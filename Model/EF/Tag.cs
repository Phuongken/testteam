namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Tag")]
    public partial class Tag
    {
        public int ID { get; set; }

        [Required]
        [StringLength(80)]
        public string name { get; set; }

        public int status { get; set; }
    }
}
