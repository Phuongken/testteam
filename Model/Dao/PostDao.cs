﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;

namespace Model.Dao
{
    public class PostDao
    {
        DataModel db = null;
        public PostDao()
        {
            db = new DataModel();
        }
        public long Insert(Post newPost)
        {
            db.Posts.Add(newPost);
            db.SaveChanges();
            return newPost.ID;
        }
        public List<Post> Index()
        {
            return db.Posts.ToList();
        }
        public Post ViewDetail(int id)
        {
            return db.Posts.Find(id);
        }
        public bool Update(Post newPost)
        {
            try
            {
                var post = db.Posts.Find(newPost.ID);
                post.title = newPost.title;
                post.content = newPost.content;
                post.avatar = newPost.avatar;
                post.createBy = newPost.createBy;
                post.updateBy = newPost.updateBy;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                var post = db.Posts.Find(id);
                db.Posts.Remove(post);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
