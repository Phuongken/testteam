﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.Dao;
using Model.EF;

namespace Newspaper.Areas.Admin.Controllers
{
    public class PostController : Controller
    {
        // GET: Admin/Post
        public ActionResult Index()
        {
            var dao = new PostDao();
            var model = dao.Index();
            return View(model);
        }

        // GET: Admin/Post/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        [HttpGet]
        // GET: Admin/Post/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Post/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(Post newPost)
        {
            
            if (ModelState.IsValid)
            {
                var dao = new PostDao();
                long post = dao.Insert(newPost);
                if (post > 0)
                {
                    return RedirectToAction("Index", "Post");
                }
                else
                {
                    ModelState.AddModelError("", "Thêm bài báo không thành công");
                }
            }
            return View(newPost);
        }
        [HttpGet]
        // GET: Admin/Post/Edit/5
        public ActionResult Edit(int id)
        {
            var post = new PostDao().ViewDetail(id);
            return View(post);
        }

        // POST: Admin/Post/Edit/5
        [HttpPost]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                var dao = new PostDao();
                var result = dao.Update(post);
                if (result == true)
                {
                    return RedirectToAction("Index", "Post");
                }
                else
                {
                    ModelState.AddModelError("", "Chỉnh sửa thông tin thất bại");
                }
            }
            return View(post);
        }

        // POST: Admin/Post/Delete/5
        [HttpDelete]
        public ActionResult Delete(int id)
        {
            var post = new PostDao().Delete(id);
            return RedirectToAction("Index","Post");
        }
    }
}
